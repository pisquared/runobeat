## INFRA
* [PROD] SSL
* [PROVISIONING]

## PLATFORM
* [TASKS] Refresh progress of task on the button itself if JS/Sockets is enabled
* [USER] On new user registration add default views, components etc
* [SHIPKA] Isolate shipka code completely

## APP/RUNNOBEAT
* [TASKS] YT: lastfm, spotify tasks
* [TASKS] YT: getting recommendations, duplicates
* [TASKS] YT: Import gmusic
* [MODELS] create/delete label
* [VIEWS] refactor rest of views

## FE
* [FE] [Bootstrap table](https://bootstrap-table.com/docs/getting-started/introduction/) and see the extensions

## PWA
* [SERVICE WORKERS] Offline/cache support[cookbook](https://jakearchibald.com/2014/offline-cookbook/#network-falling-back-to-cache)
* [LOCALSTORAGE] For caching mp3 files [blog](https://www.html5rocks.com/en/tutorials/offline/quota-research/), [abuser](https://demo.agektmr.com/storage/) [IndexedDB vue](https://medium.com/@mntlmaxi/make-your-vue-app-last-with-indexeddb-66f02708830e)