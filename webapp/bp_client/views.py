import os
from collections import defaultdict
from urllib import parse as urlparse

from flask import current_app as app
from flask import render_template, flash, redirect, url_for, request, send_from_directory
from flask_security import login_required, current_user

from shipka.persistance import db
from shipka.webapp.util import url_for_view
from shipka.webapp.views import build_local_ctx, hook_user_view
from webapp.bp_client import bp_client
from webapp.models import Label, Song
# from workers.lastfm import search_last_fm, track_meta_last_fm
from workers.tasks import download_youtube_song


@hook_user_view(".*")
def before_all_user_views(ctx):
    ctx['app_name'] = 'Runobeat'
    ctx['title'] = 'Runobeat'
    ctx['search_models'] = 'Song,Label'
    return ctx


@hook_user_view("listen")
def set_listen_title(ctx):
    ctx['title'] = "{} - {}".format(ctx['Song_instance'].title['value'], ctx['title'])
    return ctx


@bp_client.route('/')
@login_required
def index():
    ctx = build_local_ctx()
    return render_template("index.html", **ctx)


@bp_client.route("/songs/<int:song_id>/edit", methods=["post"])
@login_required
def edit_song(song_id):
    song = Song.filter_by(id=song_id).first()
    if not song:
        flash('No song with id {}'.format(song_id))
        return redirect(request.referer)
    song.title = request.form.get('title')
    song.indexing_title = request.form.get('indexing_title')
    song.default_start = request.form.get('default_start')
    song.default_end = request.form.get('default_end')
    db.add(song)
    db.push()
    return redirect(request.referrer)


@bp_client.route("/songs/<int:song_id>/delete")
@login_required
def delete_song(song_id):
    song = Song.filter_by(id=song_id).first()
    song_file = os.path.join(app.config.get("MEDIA_PATH"), song.filename)
    if not os.path.isfile(song_file):
        flash('No file for song with id {}'.format(song_id))
        return redirect(request.referer)
    if not song:
        flash('No song with id {}'.format(song_id))
        return redirect(request.referer)
    song.tags = []
    db.delete(song)
    db.push()
    os.remove(song_file)
    flash('Deleted with id {}'.format(song_id))
    return redirect(request.referrer)


@bp_client.route("/lastfm/search")
@login_required
def last_fm_search():
    q = request.args.get('q')
    song_id = request.args.get('song_id')
    if not q:
        return redirect(request.referrer)
    track_results = search_last_fm(q)
    return render_template("last_fm_results.html",
                           q=q,
                           song_id=song_id,
                           track_results=track_results)


@bp_client.route("/lastfm/track")
@login_required
def last_fm_track():
    q = request.args.get('q')
    res = request.args.get('res')
    song_id = request.args.get('song_id')
    if not q or not res or not res.isdigit() or int(res) < 0:
        return redirect(request.referrer)
    res = int(res)
    track_results = search_last_fm(q)
    if res > len(track_results):
        return redirect(request.referrer)
    track_meta = track_meta_last_fm(track_results[res])
    return render_template("last_fm_track.html",
                           q=q,
                           res=res,
                           track_meta=track_meta)


@bp_client.route("/song/<int:song_id>/labels/add", methods=["post"])
@login_required
def add_label_to_song(song_id):
    song = Song.filter_by(id=song_id).first()
    if not song:
        flash('No song with id {}'.format(song_id))
        return redirect(request.referrer)
    label_key = request.form.get('key')
    label_value = request.form.get('value')
    label_indexing = True if request.form.get('indexing') == "on" else False
    label = Label.create(key=label_key, value=label_value, indexing=label_indexing)
    song.labels.append(label)
    db.add(song)
    db.push()
    return redirect(url_for('client.listen', song_id=song.id))


@bp_client.route("/labels/edit", methods=["get", "post"])
@login_required
def edit_label_key_value():
    if request.method == "GET":
        key = request.args.get("k")
        value = request.args.get("v")
        label_key_value_f = "{}: {}".format(key, value)
        label_results = {label_key_value_f: {
            "count": 0,
            "key": key,
            "value": value,
        }}
        if key and value:
            label_count = Label.filter_by(key=key, value=value).count()
            label_results[label_key_value_f]["count"] = label_count
        return render_template("label_edit.html", k=key, v=value,
                               label_results=label_results)
    elif request.method == "post":
        key = request.args.get("k")
        value = request.args.get("v")
        new_value = request.args.get("nv")
        if key and value:
            labels = Label.filter_by(key=key, value=value).all()


@bp_client.route("/labels/search", methods=["get", "post"])
@login_required
def search_label_key_value():
    if request.method == "GET":
        key = request.args.get("k")
        q = request.args.get("q")
        label_results = defaultdict(lambda: {
            "count": 0,
            "key": "",
            "value": "",
        })
        if key and q:
            labels = Label.search(q).all()
            for label in labels:
                label_results["{}: {}".format(label.key, label.value)]["count"] += 1
                label_results["{}: {}".format(label.key, label.value)]["key"] = label.key
                label_results["{}: {}".format(label.key, label.value)]["value"] = label.value
        return render_template("label_search.html", k=key, q=q,
                               label_results=label_results)
    elif request.method == "post":
        key = request.args.get("k")
        q = request.args.get("q")
        new_value = request.args.get("nv")
        if key and q:
            labels = Label.filter_by(key=key, value=q).all()


@bp_client.route("/labels/<int:label_id>/edit", methods=["post"])
@login_required
def edit_label(label_id):
    label = Label.filter_by(id=label_id).first()
    if not label:
        flash('No label with id {}'.format(label_id))
        return redirect(request.referer)
    label.key = request.form.get('key')
    label.value = request.form.get('value')
    db.add(label)
    db.push()
    return redirect(request.referrer)


@bp_client.route("/labels/<int:label_id>/delete", methods=["post"])
@login_required
def delete_label(label_id):
    label = Label.filter_by(id=label_id).first()
    if not label:
        flash('No label with id {}'.format(label_id))
        return redirect(request.referer)
    db.delete(label)
    db.push()
    return redirect(request.referrer)


@bp_client.route("/search")
@login_required
def old_search():
    q = request.args.get("q")
    if not q:
        return redirect(request.referrer)
    q = q.strip()
    video_id = None
    for prefix in youtube_prefixes:
        if q.startswith(prefix):
            parsed = urlparse.urlparse(q)
            video_id = urlparse.parse_qs(parsed.query).get('v', [None])[0]
            break
    labels_by_youtube_video_id = Label.filter_by(key="youtube_video_id", value=video_id).all()
    youtube_songs = []
    for label in labels_by_youtube_video_id:
        for song in label.songs:
            youtube_songs.append(song)
    if len(youtube_songs) == 1:
        return redirect(url_for_view('listen', song_id=youtube_songs[0].id))
    if video_id:
        return render_template('search_results.html',
                               download_youtube_video=video_id,
                               youtube_songs=youtube_songs,
                               )
    song_results = []
    pre_song_results = Song.search(q).all()
    for song in pre_song_results:
        if song not in youtube_songs:
            song_results.append(song)
    labels = Label.search(q).all()
    label_results = defaultdict(list)
    for label in labels:
        for song in label.songs:
            if song in song_results:
                continue
            label_results["{}: {}".format(label.key, label.value)].append(song)

    return render_template('search_results.html',
                           download_youtube_video=video_id,
                           youtube_songs=youtube_songs,
                           label_results=label_results,
                           songs=song_results,
                           q=q,
                           )


@bp_client.route("/download")
@login_required
def download():
    q = request.args.get("q")
    youtube_label = Label.create(key="youtube_video_id", value=q)
    song = Song.create()
    song.labels.append(youtube_label)
    db.push()
    task = download_youtube_song.delay(current_user.id, song.id)
    # enrich_song_from_yt_title_to_last_fm.delay(song.id)
    # enrich_song_from_spotify.delay(song.id)
    # transliterate_title.delay(song.id)
    return redirect(url_for_view('listen',
                                 song_id=song.id,
                                 task_id=task.task_id))


@bp_client.route("/run/<int:song_id>")
@login_required
def get_run(song_id):
    song = Song.filter_by(id=song_id).first()
    bpm_round = song.label_key("bpm_round")
    return render_template("run.html",
                           bpm_round=bpm_round,
                           song=song)


@bp_client.route("/labels")
@login_required
def labels():
    label_key = request.args.get('k')
    label_value = request.args.get('v')
    all_songs, label_keys, label_values = [], [], []
    if label_key and label_value:
        labels = Label.filter_by(key=label_key, value=label_value).all()
        all_songs = []
        for label in labels:
            for song in label.songs:
                all_songs.append(song)
    elif label_key:
        labels = Label.filter_by(key=label_key).group_by(Label.value).all()
        for label in labels:
            label_values.append(label.value)
    else:
        labels = Label.query.group_by(Label.key).all()
        for label in labels:
            label_keys.append(label.key)
    return render_template("label.html",
                           label_key=label_key,
                           label_value=label_value,
                           all_songs=all_songs,
                           label_keys=label_keys,
                           label_values=label_values)


youtube_prefixes = [
    "https://www.youtube.com/watch",
    "https://m.youtube.com/watch",
]
