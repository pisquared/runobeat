from shipka.persistance import db
from shipka.persistance.database import ModelController, Datable, Deletable, Searchable
from shipka.persistance.models.auth import Ownable
from shipka.persistance.models.log import Loggable

songs_tags = db.sa_db.Table('songs_labels',
                            db.sa_db.Column('song_id', db.sa_db.Integer(), db.sa_db.ForeignKey('song.id')),
                            db.sa_db.Column('label_id', db.sa_db.Integer(), db.sa_db.ForeignKey('label.id')))


class Song(db.sa_db.Model, ModelController, Ownable, Datable, Deletable, Loggable, Searchable):
    id = db.sa_db.Column(db.sa_db.Integer, primary_key=True)

    title = db.sa_db.Column(db.sa_db.Unicode)
    indexing_title = db.sa_db.Column(db.sa_db.Unicode)
    filename = db.sa_db.Column(db.sa_db.Unicode)
    duration = db.sa_db.Column(db.sa_db.Integer)

    default_start = db.sa_db.Column(db.sa_db.Integer)
    default_end = db.sa_db.Column(db.sa_db.Integer)

    labels = db.sa_db.relationship("Label", secondary=songs_tags)

    @property
    def indexed_labels(self):
        return [label for label in self.labels if label.indexing]

    @property
    def non_indexed_labels(self):
        return [label for label in self.labels if not label.indexing]

    def label_key(self, label_key):
        for label in self.labels:
            if label.key == label_key:
                return label.value

    def label_keys(self, label_key):
        values = []
        for label in self.labels:
            if label.key == label_key:
                values.append(label.value)
        return values

    def __repr__(self):
        return "<Song {}: '{}'>".format(self.id, self.title)


class Label(db.sa_db.Model, ModelController, Ownable, Datable, Deletable, Loggable, Searchable):
    id = db.sa_db.Column(db.sa_db.Integer, primary_key=True)

    indexing = db.sa_db.Column(db.sa_db.Boolean)

    key = db.sa_db.Column(db.sa_db.Unicode)
    value = db.sa_db.Column(db.sa_db.Unicode)

    songs = db.sa_db.relationship("Song", secondary=songs_tags)

    def __repr__(self):
        return "<Label {}:{}>".format(self.key, self.value)
