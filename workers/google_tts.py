import os

from flask import current_app as app


class GoogleTTS(object):
    def __init__(self):
        print('init google tts')
        from google.cloud import texttospeech
        self.tts = texttospeech
        self.tts_client = self.tts.TextToSpeechClient.from_service_account_file(
            os.path.join(app.config.get("BASEPATH"), 'etc/secrets/pi2-tts-svc-acc.json')
        )

    def gen_speech(self, text, language_code, output_file):
        """Synthesizes speech from the input string of text or ssml.

        Note: ssml must be well-formed according to:
            https://www.w3.org/TR/speech-synthesis/
        """

        # Set the text input to be synthesized
        synthesis_input = self.tts.types.SynthesisInput(text=text)

        # Build the voice request, select the language code ("en-US") and the ssml
        # voice gender ("neutral")
        voice = self.tts.types.VoiceSelectionParams(
            language_code=language_code,
            ssml_gender=self.tts.enums.SsmlVoiceGender.NEUTRAL)

        # Select the type of audio file you want returned
        audio_config = self.tts.types.AudioConfig(
            audio_encoding=self.tts.enums.AudioEncoding.MP3)

        # Perform the text-to-speech request on the text input with the selected
        # voice parameters and audio file type
        response = self.tts_client.synthesize_speech(synthesis_input, voice, audio_config)

        # The response's audio_content is binary.
        with open(output_file, 'wb') as out:
            # Write the response to the output file.
            out.write(response.audio_content)



