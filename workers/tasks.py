import datetime
import json
import os
from collections import defaultdict
from urllib import parse as urlparse

from flask import current_app as app

from shipka.persistance import db
from shipka.persistance.models.models import Model
from shipka.persistance.models.tasks import Task
from shipka.workers import worker
from webapp.models import Song, Label

GOOGLE_TTS = None


def init_once():
    from workers.google_tts import GoogleTTS
    global GOOGLE_TTS
    GOOGLE_TTS = GoogleTTS()


class YdLogger(object):
    def __init__(self, task):
        self.info_json = {}
        self.task = task

    def warning(self, msg):
        pass

    def error(self, msg):
        pass

    def debug(self, msg):
        try:
            self.info_json = json.loads(msg)
        except:
            print(msg)
            if '% of ' in msg:
                complete = msg.split('% of ')[0].split()[-1]
                self.task.update(**dict(
                    progress="Downloading...",
                    percent_done=int(float(complete)),
                ))
                db.push()


youtube_prefixes = [
    "https://www.youtube.com/watch",
    "https://m.youtube.com/watch",
]


@worker.task(name='download_youtube_song', bind=True)
def download_youtube_song(self, video_url=None, listen_url=None, *args, **kwargs):
    import youtube_dl

    task = Task.get_one_by(uuid=self.request.id)

    video_id = None
    for prefix in youtube_prefixes:
        if video_url.startswith(prefix):
            parsed = urlparse.urlparse(video_url)
            video_id = urlparse.parse_qs(parsed.query).get('v', [None])[0]
            break
    song = Model.create_instance("Song", fields=[])
    media_fpath = os.path.join(app.config.get("MEDIA_PATH"), song.uuid)
    yd_logger = YdLogger(task)
    ydl_opts = {
        'outtmpl': "{}.%(ext)s".format(media_fpath),
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
        'forcejson': True,
        'logger': yd_logger,
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download(['http://www.youtube.com/watch?v={}'.format(video_id)])
    updates = {
        "title": yd_logger.info_json.get('title'),
        "audio_file": "{}.mp3".format(song.uuid),
        "duration": str(yd_logger.info_json.get('duration')),
    }
    fields = Model.dict_to_fields("Song", updates)
    Model.update_instances("Song", updates=fields, instance_uuid=song.uuid)

    return dict(
        next_url="{}?instance_uuid={}".format(listen_url, song.uuid),
    )


@worker.task(name='get_tts', bind=True)
def get_tts(self, instance_uuid=None, lang=None, text=None, learn_url=None, *args, **kwargs):
    global GOOGLE_TTS
    task = Task.get_one_by(uuid=self.request.id)

    updates = {}
    fname = "{}_{}.mp3".format(lang, instance_uuid)
    fpath = os.path.join(app.config.get("MEDIA_PATH"), fname)
    GOOGLE_TTS.gen_speech(text=text, language_code=lang, output_file=fpath)
    updates["{}_sound".format(lang)] = fname

    fields = Model.dict_to_fields("Word", updates)
    Model.update_instances("Word", updates=fields, instance_uuid=instance_uuid)

    task.update(**dict(
        progress="Done",
        percent_done=100,
    ))
    db.push()

    return dict(
        next_url=learn_url
    )


############### OLD


@worker.task
def transliterate_title(song_id):
    from transliterate import translit
    song = Song.get_one_by(id=song_id)
    song_title = song.title
    try:
        indexing_title = translit(song_title, 'bg', reversed=True)
        if indexing_title:
            song.indexing_title = indexing_title
    except Exception as e:
        pass


def duplicate_from_song(this_song):
    this_song_duplicates = this_song.label_key('duplicate_ids')
    duplicates = []
    if this_song_duplicates:
        duplicate_ids = this_song_duplicates.split()
        try:
            for dupl_id in duplicate_ids:
                song = Song.query.filter_by(id=dupl_id).first()
                if song:
                    duplicates.append(song)
        except:
            # TODO: raise excpetion
            pass
        return duplicates
    this_song_artist = this_song.label_key('artist')
    this_song_title = this_song.label_key('title')
    if not (this_song_artist and this_song_title):
        return []

    all_songs = Song.query.all()
    for other_song in all_songs:
        if this_song == other_song:
            continue
        if compare_titles(this_song_artist, this_song_title, other_song):
            duplicates.append(other_song)
    if not duplicates:
        this_song.labels.append(Label.create(key="duplicate_ids",
                                             value='-1', indexing=False))
    else:
        this_song.labels.append(Label.create(key="duplicate_ids",
                                             value=' '.join([str(x.id) for x in duplicates]),
                                             indexing=False))
    this_song.labels.append(Label.create(key="duplicate_calc_dt",
                                         value=str(datetime.datetime.utcnow()),
                                         indexing=False))
    db.add(this_song)
    db.push()
    return duplicates


def recommend_from_song(this_song, song_duplicates):
    recomended_songs = defaultdict(lambda: {
        'song_id': 0,
        'score': 0,
        'last_played': 0,
        'reasons': defaultdict(int)
    })
    # TODO: This could be optimized with joins
    for label in this_song.indexed_labels:
        other_labels = Label.query.filter_by(key=label.key, value=label.value).all()
        for other_label in other_labels:
            for other_song in other_label.songs:
                if this_song == other_song or other_song in song_duplicates:
                    continue
                last_played = other_song.label_key('last_played')
                recomended_songs[other_song.id]['song_id'] = other_song.id
                recomended_songs[other_song.id]['title'] = other_song.title
                recomended_songs[other_song.id]['score'] += 1
                recomended_songs[other_song.id]['last_played'] = last_played or '0'
                recomended_songs[other_song.id]['reasons'][label.key] += 1
    # sort MAX_LIST number of high-scoring songs, then rev sort by last played
    score_cut = sorted(recomended_songs.values(), key=lambda x: -x['score'])[:MAX_LIST]
    return sorted(score_cut, key=lambda x: x['last_played'])


def log_song_played(song):
    now = str(datetime.datetime.utcnow())
    play_count_label, last_played_label = None, None
    for label in song.labels:
        if label.key == 'last_played':
            last_played_label = label
        if label.key == 'play_count':
            play_count_label = label
    if not play_count_label:
        play_count_label = Label.create(key="play_count", value="1", indexing=False)
    else:
        prev_play_count = play_count_label.value
        if prev_play_count and prev_play_count.isdigit():
            prev_play_count = int(prev_play_count)
            play_count_label.value = str(prev_play_count + 1)
            db.add(play_count_label)
    if not last_played_label:
        last_played_label = Label.create(key="last_played",
                                         value=now,
                                         indexing=False)
    else:
        last_played_label.value = now
    song.labels.append(play_count_label)
    song.labels.append(last_played_label)
    db.add(song)
    db.push()
