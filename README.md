# Runobeat

*Run with the beat of your favourite music*


Other features:
* Add any music video from youtube
* Organize your music with key=value labels
* Infinite loop between certain seconds of video

## Run:

The app consists of webapp, queue and workers. They can either be run as docker containers or standalone.

### Queue

Make sure you have Docker installed (if you decide to go the docker path):

```bash
./INSTALL_DOCKER.sh
```

Then, to run the queue:

```bash
./RUN_QUEUE_DOCKER.sh
```

Alternatively, install rabbitmq package for your distribution.

### Workers

```bash
./RUN_WORKERS.sh
```


### Webapp
You need to ask admin for secret files. Then:

Either run the webapp with docker:

```bash
./RUN_WEB_DOCKER.sh
```

or on your own machine, you need to install requirements first:

```bash
./INSTALL.sh
```

and:

```bash
./RUN_DEV.sh
```

## Login

Default admin username and password:

```
user: admin@runobeat.com
pass: password
```

