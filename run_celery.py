#!/usr/bin/env python3
from shipka.workers import worker

if __name__ == '__main__':
    worker.worker_main(argv=['worker', '-lINFO'])
