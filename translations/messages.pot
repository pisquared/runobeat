# Translations template for PROJECT.
# Copyright (C) 2019 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2019-04-26 09:46+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

#: apps/runobeat/templates/listen_song.html:3
#: webapp/components/listen/templates/main.html:3
msgid "Run with"
msgstr ""

#: apps/runobeat/templates/listen_song.html:4
#: webapp/components/listen/templates/main.html:4
msgid "Play through"
msgstr ""

#: apps/runobeat/templates/listen_song.html:5
#: webapp/components/listen/templates/main.html:5
msgid "Duration:"
msgstr ""

#: shipka/webapp/templates/_macros.html:88
msgid "Select file"
msgstr ""

#: shipka/webapp/templates/navbar.html:5 shipka/webapp/templates/navbar.html:7
msgid "Search"
msgstr ""

#: shipka/webapp/templates/navbar.html:49
msgid "Log out"
msgstr ""

#: shipka/webapp/templates/forms/login.html:11
#: shipka/webapp/templates/navbar.html:55
msgid "Login"
msgstr ""

#: shipka/webapp/templates/forms/register.html:9
#: shipka/webapp/templates/navbar.html:56
#: shipka/webapp/templates/override_security_templates/register_user.html:22
msgid "Register"
msgstr ""

#: shipka/webapp/templates/translations.html:1
msgid "Translations"
msgstr ""

#: shipka/webapp/templates/translations.html:11
msgid "Hello world"
msgstr ""

#: shipka/webapp/templates/translations.html:12
#: shipka/webapp/templates/translations.html:13
#, python-format
msgid "Hello %(num)s world"
msgid_plural "Hello %(num)s worlds"
msgstr[0] ""
msgstr[1] ""

#: shipka/webapp/templates/translations.html:17
#, python-format
msgid ""
"\n"
"            Hello %(user)s! This is a blockquote translations.\n"
"        "
msgstr ""

#: shipka/webapp/templates/translations.html:25
#, python-format
msgid ""
"\n"
"            There is %(count)s object.\n"
"            "
msgid_plural ""
"\n"
"            There are %(count)s objects.\n"
"        "
msgstr[0] ""
msgstr[1] ""

#: shipka/webapp/templates/view_model_delete_confirm.html:14
#: shipka/webapp/templates/view_model_single.html:15
msgid "Delete"
msgstr ""

#: shipka/webapp/templates/view_model_edit.html:15
#: shipka/webapp/templates/view_model_new.html:14
msgid "Save"
msgstr ""

#: shipka/webapp/templates/view_model_many.html:12
msgid "New"
msgstr ""

#: shipka/webapp/templates/view_model_single.html:12
msgid "Edit"
msgstr ""

#: shipka/webapp/templates/forms/login.html:8
msgid "Remember me"
msgstr ""

#: shipka/webapp/templates/override_security_templates/change_password.html:9
msgid "You are using a temporarily generated password. Please change below"
msgstr ""

#: shipka/webapp/templates/override_security_templates/change_password.html:13
msgid "Change password"
msgstr ""

#: shipka/webapp/templates/override_security_templates/change_password.html:18
msgid "Temporary password"
msgstr ""

#: shipka/webapp/templates/override_security_templates/forgot_password.html:8
msgid "Send password reset instructions"
msgstr ""

#: shipka/webapp/templates/override_security_templates/login_user.html:22
msgid "Log in"
msgstr ""

#: shipka/webapp/templates/override_security_templates/send_confirmation.html:13
msgid "Resend confirmation instructions"
msgstr ""

#: webapp/strings.py:3
msgid "Listen"
msgstr ""

#: webapp/strings.py:4
msgid "Run"
msgstr ""

#: webapp/strings.py:5
msgid "Learn"
msgstr ""

#: webapp/strings.py:6
msgid "English"
msgstr ""

#: webapp/strings.py:7
msgid "Bulgarian"
msgstr ""

#: webapp/strings.py:8
msgid "German"
msgstr ""

#: webapp/bp_client/templates/index.html:7
msgid "Welcome to Runobeat!"
msgstr ""

#: webapp/components/all_labels/templates/main.html:1
#: webapp/components/all_songs/templates/main.html:1
msgid "All songs"
msgstr ""

#: webapp/components/all_labels/templates/main.html:2
msgid "Labels"
msgstr ""

