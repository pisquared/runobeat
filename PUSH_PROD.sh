#!/usr/bin/env bash
#newgrp docker
docker build -t gcr.io/pisquared-220114/runobeat .
docker build -t gcr.io/pisquared-220114/runobeat_workers -f Dockerfile.workers  .
docker login -u oauth2accesstoken -p "$(gcloud auth print-access-token)" https://gcr.io
docker push gcr.io/pisquared-220114/runobeat
kubectl apply -f etc/provision/prod/runobeat_pv.yaml \
              -f etc/provision/prod/runobeat_deployment.yaml \
              -f etc/provision/prod/runobeat_workers_deployment.yaml \
              -f etc/provision/prod/runobeat_service.yaml \
              -f etc/provision/prod/runobeat_workers_service.yaml \
              -f etc/provision/prod/secrets/runobeat_secrets.yaml