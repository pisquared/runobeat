FROM python:3

WORKDIR /appa

COPY deb_packages.txt ./
COPY requirements.txt ./
COPY $SHIPKA_PATH ./
RUN ln -s /appa/shipka/shipka.sh /usr/bin/shipka
RUN shipka install

#COPY . .

ENV WEBAPP_ENV=prod WEBAPP_HOST="0.0.0.0" WEBAPP_PORT="5001" CELERY_BROKER_URL="amqp://guest@runobeat-queue:5672/"
CMD [ "shipka", "run-dev" ]