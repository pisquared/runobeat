#!/usr/bin/env bash
# docker volume prune
# newgrp docker
docker build -f Dockerfile.workers -t runobeat_workers:1.0 .
docker tag runobeat_workers:1.0 gcr.io/pisquared-220114/runobeat_workers

docker run --rm \
  --name runobeat_workers \
  --mount source=runobeat-database,target=/app/database \
  --mount source=runobeat-media,target=/app/media \
  --net queuenet \
  runobeat_workers:1.0
