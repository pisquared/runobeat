#!/usr/bin/env bash
# docker volume prune
# newgrp docker
docker build -t runobeat:1.0 .
docker tag runobeat:1.0 gcr.io/pisquared-220114/runobeat

docker volume create runobeat-database
docker volume create runobeat-media
docker volume create runobeat-search-index

docker run --rm \
  --name runobeat \
  --mount source=runobeat-database,target=/app/database \
  --mount source=runobeat-media,target=/app/media \
  --mount source=runobeat-search-index,target=/app/search_index \
  -p 5001:5001 \
  --net queuenet \
  runobeat:1.0
