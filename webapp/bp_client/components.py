from flask import render_template, flash, request

from shipka.persistance.models.models import Model
from shipka.webapp.views import register_component, component_requirement, RequirementType


@register_component("listen")
@component_requirement(type=RequirementType.ModelInstance,
                       model_name="Song",
                       instance_uuid_from=RequirementType.from_request_args, )
def component_listen(ctx):
    song = ctx['Song_instance']
    if song:
        labels = song.labels()
        ctx['label_model'] = labels.model
        ctx['song_labels'] = labels.instances
    return render_template("listen/listen.html", **ctx), [
        render_template("listen/listen.js", **ctx)
    ]


@register_component("run")
@component_requirement(type=RequirementType.ModelInstance,
                       model_name="Song",
                       instance_uuid_from=RequirementType.from_request_args, )
def component_run(ctx):
    song = ctx['Song_instance']
    if song:
        labels = song.labels()
        ctx['label_model'] = labels.model
        ctx['song_labels'] = labels.instances
        for label in labels.instances:
            if label.key == 'bpm':
                ctx['bpm'] = label.value
    return render_template("run/run.html", **ctx), [
        render_template("run/run.js", **ctx)
    ]


@register_component("all_songs")
def component_all_songs(ctx):
    all_songs = Model.query(model_name="Song")
    return render_template("all_songs/all_songs.html", all_songs=all_songs), []


@register_component("all_labels")
def component_all_labels(ctx):
    label_keys = []
    labels_model = Model.query(model_name="Label")
    for label in labels_model.instances:
        label_keys.append(label.key.get('selection', {}).get('key'))
    return render_template("all_labels/all_labels.html",
                           label_keys=set(label_keys),
                           ), []


@register_component("new_word")
def component_new_word(ctx):
    ctx['word_model'] = Model.query(model_name="Word").model
    return render_template("new_word/new_word.html", **ctx), []


@register_component("word_list")
def component_word_list(ctx):
    ctx['word_instances'] = Model.query(model_name="Word").instances
    return render_template("new_word/word_list.html", **ctx), []
