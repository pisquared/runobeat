#!/usr/bin/env bash
docker network create queuenet
docker run --rm --name runobeat-queue -p 5672:5672 --net queuenet rabbitmq:3