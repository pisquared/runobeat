#!/usr/bin/env bash
set -e
VENV_DIR=${VENV_DIR:-venv}
PID_DIR=${PID_DIR:-.}
STATE_DIR=${STATE_DIR:-.}


echo $$ >> ${PID_DIR}/celery.pid
source "${VENV_DIR}/bin/activate"
celery -A shipka.workers.worker worker -l info --statedb=${STATE_DIR}/celery_worker.state