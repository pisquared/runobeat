var BPM = "{{ bpm or 120 }}";
var AUDIO_SRC = "{{ url_for('get_media', filepath=Song_instance.audio_file.value) }}";
var audioComponent = new Vue({
    el: '#audio-component',
    data: {
        bpm: BPM,
        runningSpeed: 0,
        audioSrc: AUDIO_SRC,
        audioPlaying: true,
        audioCurrentTime: 0,
        audioDuration: 0,
        metronomeInterval: undefined,
    },
    watch: {
        audioCurrentTime(time) {
            if (Math.abs(time - this.$refs.audio.currentTime) > 0.5) {
                this.$refs.audio.currentTime = time.toFixed(0);
            }
        }
    },
    computed: {
        computedInitialRunningSpeed: function () {
            return (this.bpm - 60) / 10;
        },
        computedPlaybackSpeed: function () {
            return (this.computedBpm / this.bpm).toFixed(2);
        },
        computedBpm: function () {
            return (this.runningSpeed * 10) + 60;
        },
        computePlayButton: function () {
            if (this.audioPlaying) {
                return "fas fa-pause"
            } else {
                return "fas fa-play"
            }
        },
        metronome: function () {
            if (this.metronomeInterval) {
                clearInterval(this.metronomeInterval);
            }
            var BPM_ms = 1000 / (this.computedBpm / 60);
            this.metronomeInterval = setInterval(function () {
                $("#metronome-left").toggleClass("flash-red");
                $("#metronome-right").toggleClass("flash-blue");
            }.bind(this), BPM_ms);
        }
    },
    methods: {
        decreaseRunningSpeed: function (value) {
            this.runningSpeed -= value;
            this.metronome;
        },
        increaseRunningSpeed: function (value) {
            this.runningSpeed += value;
            this.metronome;
        },
        resetAudioPlayingSpeed: function () {
            this.runningSpeed = this.initialRunningSpeed;
        },
        playPauseAudio: function () {
            this.audioPlaying = !this.audioPlaying;
        },
        replayAudio: function () {
            this.audioCurrentTime = 0;
            this.audioPlaying = true;
        },
        updateAudioCurrentTime: function (e) {
            this.audioCurrentTime = e.target.currentTime.toFixed(0);
        },
        updateAudioDuration: function () {
            this.runningSpeed = this.computedInitialRunningSpeed;
            this.metronome;
            this.audioDuration = this.$refs.audio.duration.toFixed(0);
        },
        floatRound: function (value) {
            return parseFloat(value).toFixed(1);
        },
        secondsToString: function (totalSeconds) {
            var hours = Math.floor(totalSeconds / 3600);
            var minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
            var seconds = totalSeconds - (hours * 3600) - (minutes * 60);

            // round seconds
            seconds = Math.round(seconds * 100) / 100;

            var result = (hours !== 0 ? (hours < 10 ? "0" + hours : hours) + ":" : "");
            result += (minutes < 10 ? "0" + minutes : minutes);
            result += ":" + (seconds < 10 ? "0" + seconds : seconds);
            return result;
        }
    },
    directives: {
        playing(el, binding) {
            if (!binding.value) el.pause(); else el.play();
        },
        playbackRate(el, binding) {
            el.playbackRate = binding.value;
        }
    }
})