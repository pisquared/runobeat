import json
import os

import yaml


def _create_admin(email, password):
    from flask_security.utils import hash_password
    from shipka.persistance import db
    from shipka.persistance.models.auth import Role, User, Constants

    # create admin role
    admin_role = Role.get_or_create(name=Constants.ADMIN_ROLE)
    admin_user = User.get_one_by(email=email)

    if not admin_user:
        admin_user = User.create(email=email,
                                 name=u"Admin",
                                 password=hash_password(password))
        admin_user.roles = [admin_role]
        db.add(admin_user)

    normal_user = User.get_one_by(email="user@runobeat.com")
    if not normal_user:
        User.create(email="user@runobeat.com",
                    name="User",
                    password=hash_password(password))
    return admin_user, hasattr(admin_role, 'is_new')


def create_user_views(app, admin_user):
    from shipka.persistance.models.app import UserView, UserComponent

    views_path = os.path.join(app.config.get("BASEPATH"), 'apps', 'runobeat', 'views.yaml')
    with open(views_path) as f:
        for document in yaml.safe_load_all(f):
            for view_def in document.get('views'):
                components = view_def.pop("components")
                if view_def.get('parent_user_view'):
                    parent_user_view_url_name = view_def.pop('parent_user_view')
                    parent_user_view = UserView.get_one_by(url_name=parent_user_view_url_name)
                    view_def['parent_user_view'] = parent_user_view
                uv = UserView.get_or_create(
                    user=admin_user,
                    **view_def,
                )
                for component_def in components:
                    UserComponent.get_or_create(
                        user=admin_user,
                        user_view=uv,
                        **component_def
                    )


def create_models(app):
    from shipka.persistance.models.models import META_MODEL_METHODS
    for app_name in ['runobeat']:
        install_app = os.path.join(app.config.get("BASEPATH"),
                                    'apps',
                                    app_name)
        for meta_model in ["models", "instances"]:
            with open(os.path.join(install_app, "{}.yaml".format(meta_model))) as f:
                for data in yaml.safe_load_all(f):
                    method = META_MODEL_METHODS[meta_model]
                    method(data.get(meta_model, []))
                    rel_method = META_MODEL_METHODS["{}_relationships".format(meta_model)]
                    rel_method(data.get("relationships"))


def populate(app):
    from shipka.persistance import db
    with app.app_context():
        sensitive_config = json.load(open(os.path.join(app.config.get("SECRETS_PATH"), 'app_sensitive.json')))
        admin_user, is_new = _create_admin(email=sensitive_config.get("admin_webapp_user"),
                                           password=sensitive_config.get('admin_webapp_password'))
        if is_new:
            from shipka.persistance.models.auth import global_user
            with global_user(admin_user):
                create_user_views(app, admin_user)
                create_models(app)
        db.push()
